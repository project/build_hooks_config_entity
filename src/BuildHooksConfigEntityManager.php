<?php

namespace Drupal\build_hooks_config_entity;

use Drupal\build_hooks\DeployLogger;
use Drupal\build_hooks\Trigger;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Logger\LoggerChannelTrait;
use Drupal\Core\Messenger\MessengerTrait;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Class AzureManager.
 */
class BuildHooksConfigEntityManager {

  use StringTranslationTrait;
  use MessengerTrait;
  use LoggerChannelTrait;


  /**
   * Drupal\Core\Config\ConfigFactoryInterface definition.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * A build hooks deploy logger service.
   *
   * @var \Drupal\build_hooks\DeployLogger
   */
  protected $buildHooksDeployLogger;

  /**
   * A build hooks trigger service.
   *
   * @var \Drupal\build_hooks\Trigger
   */
  protected $buildHooksTrigger;

  /**
   * BuildHooksConfigEntityManager constructor.
   *
   * {@inheritDoc}
   */
  public function __construct(ConfigFactoryInterface $config_factory, DeployLogger $build_hooks_deploylogger, Trigger $build_hooks_trigger) {
    $this->configFactory = $config_factory;
    $this->buildHooksDeployLogger = $build_hooks_deploylogger;
    $this->buildHooksTrigger = $build_hooks_trigger;
  }

  /**
   * Test if the config entity can trigger build hooks.
   *
   * @param \Drupal\Core\Config\Entity\ConfigEntityInterface $entity
   *   The config entity.
   *
   * @return bool
   *   True if can trigger build hooks.
   */
  public function isEntityTypeLoggable(ConfigEntityInterface $entity) {
    $settings = $this->configFactory->get('build_hooks_config_entity.settings');
    $config_entities = $settings->get('config_entities');
    if (!empty($config_entities[$entity->getEntityTypeId()]) && $config_entities[$entity->getEntityTypeId()] == $entity->getEntityTypeId()) {
      return TRUE;
    }
    return FALSE;
  }

  /**
   * Trigger build hooks on config entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The config entity.
   * @param string $operation
   *   The current operation of entity.
   *
   * @return bool
   *   False if can't trigger.
   */
  public function deployConfigEntity(EntityInterface $entity, $operation) {

    if (!$entity instanceof ConfigEntityInterface) {
      return FALSE;
    }
    if (!$this->isEntityTypeLoggable($entity)) {
      return FALSE;
    }

    // Log the entity.
    $this->getLogger('build_hooks_config_entity')
      ->info('@entityBundle: %entityTitle was %operation.', [
        '@entityBundle' => $entity->bundle(),
        '%entityTitle' => $entity->label(),
        '%operation' => $operation,
      ]);
    $this->buildHooksTrigger->deployFrontendEntityUpdateEnvironments();
    $this->buildHooksTrigger->invalidateToolbarCacheTag();
    return TRUE;
  }

}
