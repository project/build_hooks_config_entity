<?php

namespace Drupal\build_hooks_config_entity\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class BuildHooksCircleCiConfigForm.
 */
class ConfigEntityConfigForm extends ConfigFormBase {

  protected $entityTypeManager;

  /**
   * ConfigEntityConfigForm constructor.
   *
   * {@inheritdoc}.
   */
  public function __construct(ConfigFactoryInterface $config_factory, EntityTypeManagerInterface $entity_type_manager) {
    parent::__construct($config_factory);
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'build_hooks_config_entity.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'build_hooks_config_entity_config_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $options = [];
    $config = $this->config('build_hooks_config_entity.settings');
    $definitions = $this->entityTypeManager->getDefinitions();
    foreach ($definitions as $entity_type => $definition) {
      if ($definition->entityClassImplements(ConfigEntityInterface::class)) {
        $options[$entity_type] = $definition->getLabel();
      }
    }
    asort($options);
    $form['config_entities'] = [
      '#type' => 'select',
      '#options' => $options,
      '#multiple' => TRUE,
      '#title' => $this->t('Config entities'),
      '#description' => $this->t('Select the config entities type that you want to trigger build when they are created, updated and deleted.'),
      '#required' => TRUE,
      '#default_value' => $config->get('config_entities'),
    ];
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    // Save the api key to configuration:
    $this->config('build_hooks_config_entity.settings')
      ->set('config_entities', $form_state->getValue('config_entities'))
      ->save();
  }

}
